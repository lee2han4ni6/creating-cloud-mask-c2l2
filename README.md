# Creating Cloud Mask - C2L2

This code does cloud masking on QA band, Band 4, 5 and 10 from Landsat 8 Collection 2 Level 2 satellite imagery for LST calculation.
